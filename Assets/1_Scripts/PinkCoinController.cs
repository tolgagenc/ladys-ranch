using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PinkCoinController : MonoBehaviour
{
    public int DelayAmount = 6; // Second count
    protected float Timer;

    GameObject coinTxt;

    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("Pink Coin"))
        {
            PlayerPrefs.SetInt("Pink Coin", 0);
        }
        else
        {
            var temp = Convert.ToInt64(PlayerPrefs.GetString("Exit Time"));

            DateTime oldDate = DateTime.FromBinary(temp);

            Preferences.pinkCoin = PlayerPrefs.GetInt("Pink Coin") + ((int)DateTime.Now.Subtract(oldDate).TotalSeconds) / 6;
        }

        coinTxt = transform.GetChild(1).gameObject;
        coinTxt.GetComponent<TextMeshProUGUI>().text = Preferences.blueCoin.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        Timer += Time.deltaTime;

        if (Timer >= DelayAmount)
        {
            Timer = 0f;
            Preferences.pinkCoin += Preferences.pinkCoinAdd * Preferences.storedPinkSlime;
        }

        coinTxt.GetComponent<TextMeshProUGUI>().text = Preferences.pinkCoin.ToString();
    }
}
