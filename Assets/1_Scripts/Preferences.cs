using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Preferences
{
    public static int pinkSlime = 0;
    public static int blueSlime = 0;
    public static int goldSlime = 0;

    public static int pinkCoin = 0;
    public static int blueCoin = 0;
    public static int goldCoin = 0;

    public static int pinkCoinAdd = 1;
    public static int blueCoinAdd = 1;
    public static int goldCoinAdd = 1;

    public static int storedPinkSlime = 0;
    public static int storedBlueSlime = 0;
    public static int storedGoldSlime = 0;

    public static bool blue = false;
    public static bool gold = false;
}
