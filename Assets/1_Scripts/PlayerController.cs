using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public CharacterController controller;

    RaycastHit hitInfo;

    public Transform cam;

    public float speed = 6f;
    public float turnSmoothTime = 0.1f;
    public float turnSmoothVelocity;

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);

        if ( direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

            if(Input.GetKeyDown(KeyCode.LeftShift))
            {
                controller.Move(moveDir.normalized * speed * 10 * Time.deltaTime);
            }
            else
            {
                controller.Move(moveDir.normalized * speed * Time.deltaTime);
            }
        }

        if (Physics.Raycast(r, out hitInfo))
        {
            if (hitInfo.transform.tag == "Pink Slime")
            {
                if (Input.GetKeyDown(KeyCode.F) && ( Preferences.pinkSlime < PlayerPrefs.GetInt("Pink Slime Total") ) )
                {
                    Preferences.pinkSlime++;
                    Destroy(hitInfo.transform.gameObject);
                }
            }
            if (hitInfo.transform.tag == "Blue Slime")
            {
                if (Input.GetKeyDown(KeyCode.F) && (Preferences.blueSlime < PlayerPrefs.GetInt("Blue Slime Total")))
                {
                    Preferences.blueSlime++;
                    Destroy(hitInfo.transform.gameObject);
                }
            }
            if (hitInfo.transform.tag == "Gold Slime" && (Preferences.goldSlime < PlayerPrefs.GetInt("Gold Slime Total")))
            {
                if (Input.GetKeyDown(KeyCode.F))
                {
                    Preferences.goldSlime++;
                    Destroy(hitInfo.transform.gameObject);
                }
            }
            if (hitInfo.transform.tag == "Pink Ranch")
            {
                if (Input.GetKeyDown(KeyCode.E) && (Preferences.pinkSlime > 0))
                {
                    Preferences.storedPinkSlime++;
                    Preferences.pinkSlime--;
                }
            }
            if (hitInfo.transform.tag == "Blue Ranch")
            {
                if (Input.GetKeyDown(KeyCode.E) && (Preferences.blueSlime > 0))
                {
                    Preferences.storedBlueSlime++;
                    Preferences.blueSlime--;
                }
            }
            if (hitInfo.transform.tag == "Gold Ranch")
            {
                if (Input.GetKeyDown(KeyCode.E) && (Preferences.goldSlime > 0))
                {
                    Preferences.storedGoldSlime++;
                    Preferences.goldSlime--;
                }
            }
        }
    }
}
