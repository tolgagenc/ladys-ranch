using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreController : MonoBehaviour
{
    bool pause = false;

    [SerializeField]
    GameObject pinkMenu, blueMenu, goldMenu;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Pink Panel Toggle
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (!pause)
            {
                Time.timeScale = 0f;
                pause = !pause;
                pinkMenu.SetActive(pause);
            }
            else
            {
                Time.timeScale = 1f;
                pause = !pause;
                pinkMenu.SetActive(pause);
            }
        }

        // Blue Panel Toggle
        if ( Input.GetKeyDown(KeyCode.B) && Preferences.blue )
        {
            if ( !pause ) 
            {
                Time.timeScale = 0f;
                pause = !pause;
                blueMenu.SetActive(pause);
            }
            else
            {
                Time.timeScale = 1f;
                pause = !pause;
                blueMenu.SetActive(pause);
            }
        }

        // Gold Panel Toggle
        if ( Input.GetKeyDown(KeyCode.G) && Preferences.gold)
        {
            if ( !pause )
            {
                Time.timeScale = 0f;
                pause = !pause;
                goldMenu.SetActive(pause);
            }
            else
            {
                Time.timeScale = 1f;
                pause = !pause;
                goldMenu.SetActive(pause);
            }
        }
    }

    public void pinkPanelOpen()
    {
        if ( !pause )
        {
            Time.timeScale = 0f;
            pause = !pause;
            pinkMenu.SetActive(pause);
        }
        else
        {
            Time.timeScale = 1f;
            pause = !pause;
            pinkMenu.SetActive(pause);
        }
    }
    public void bluePanelOpen()
    {
        if ( !pause && Preferences.blue )
        {
            Time.timeScale = 0f;
            pause = !pause;
            blueMenu.SetActive(pause);
        }
        else
        {
            Time.timeScale = 1f;
            pause = !pause;
            blueMenu.SetActive(pause);
        }
    }
    public void goldPanelOpen()
    {
        if ( !pause && Preferences.gold )
        {
            Time.timeScale = 0f;
            pause = !pause;
            goldMenu.SetActive(pause);
        }
        else
        {
            Time.timeScale = 1f;
            pause = !pause;
            goldMenu.SetActive(pause);
        }
    }

    public void openBlueSlime()
    {
        PlayerPrefs.SetInt("Blue", 1);
        Preferences.blue = true;
        Preferences.pinkCoin -= 100000;
    }

    public void openGoldSlime()
    {
        PlayerPrefs.SetInt("Gold", 1);
        Preferences.gold = true;
        Preferences.blueCoin -= 100000;
    }

    public void pinkSlimeStorage()
    {
        PlayerPrefs.SetInt("Pink Slime Total", PlayerPrefs.GetInt("Pink Slime Total") + 10);
        Preferences.pinkCoin -= 100000;
    }

    public void pinkSlimeCoin()
    {
        Preferences.pinkCoinAdd++;
        PlayerPrefs.SetInt("Pink Slime Coin Add", Preferences.pinkCoinAdd);
        Preferences.pinkCoin -= 100000;
    }

    public void blueSlimeCoin()
    {
        PlayerPrefs.SetInt("Blue Slime Total", PlayerPrefs.GetInt("Blue Slime Total") + 10);
        Preferences.blueCoin -= 100000;
    }

    public void goldSlimeCoin()
    {
        PlayerPrefs.SetInt("Gold Slime Total", PlayerPrefs.GetInt("Gold Slime Total") + 10);
        Preferences.goldCoin -= 100000;
    }
}
