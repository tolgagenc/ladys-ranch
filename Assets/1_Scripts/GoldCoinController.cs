using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GoldCoinController : MonoBehaviour
{
    public int DelayAmount = 6; // Second count
    protected float Timer;

    GameObject coinTxt;

    [SerializeField]
    GameObject button;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Preferences.gold && !transform.GetChild(0).gameObject.activeSelf)
        {
            if (!PlayerPrefs.HasKey("Gold Coin"))
            {
                PlayerPrefs.SetInt("Gold Coin", 0);
            }
            else
            {
                var temp = Convert.ToInt64(PlayerPrefs.GetString("Exit Time"));

                DateTime oldDate = DateTime.FromBinary(temp);

                Preferences.pinkCoin = PlayerPrefs.GetInt("Gold Coin") + ((int)DateTime.Now.Subtract(oldDate).TotalSeconds) / 6;
            }

            button.SetActive(true);

            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(1).gameObject.SetActive(true);

            coinTxt = transform.GetChild(1).gameObject;
            coinTxt.GetComponent<TextMeshProUGUI>().text = Preferences.blueCoin.ToString();
        }

        if ( Preferences.gold )
        {
            Timer += Time.deltaTime;

            if (Timer >= DelayAmount)
            {
                Timer = 0f;
                Preferences.goldCoin += Preferences.goldCoinAdd * Preferences.storedGoldSlime;
            }

            coinTxt.GetComponent<TextMeshProUGUI>().text = Preferences.goldCoin.ToString();
        }
    }
}
