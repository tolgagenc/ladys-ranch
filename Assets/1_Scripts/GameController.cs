using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public TextMeshProUGUI pink;
    public TextMeshProUGUI blue;
    public TextMeshProUGUI gold;

    void Awake()
    {
        LoadSaveData();
    }

    // Start is called before the first frame update
    void Start()
    {
        pink.text = Preferences.pinkSlime.ToString() + '/' + PlayerPrefs.GetInt("Pink Slime Total");
        
        if ( Preferences.blue )
        {
            blue.gameObject.transform.parent.gameObject.SetActive(true);            
            blue.text = "Blue Slime: " + Preferences.blueSlime + '/' + PlayerPrefs.GetInt("Blue Slime Total");
        }

        if ( Preferences.gold )
        {
            gold.gameObject.transform.parent.gameObject.SetActive(true);
            gold.text = Preferences.goldSlime.ToString() + '/' + PlayerPrefs.GetInt("Gold Slime Total");
        }
    }

    // Update is called once per frame
    void Update()
    {
        pink.text = Preferences.pinkSlime.ToString() + '/' + PlayerPrefs.GetInt("Pink Slime Total");

        if (Preferences.blue)
        {
            if ( !blue.gameObject.transform.parent.gameObject.activeSelf )
            {
                blue.gameObject.transform.parent.gameObject.SetActive(true);
            }

            blue.text = Preferences.blueSlime.ToString() + '/' + PlayerPrefs.GetInt("Blue Slime Total");
        }

        if (Preferences.gold)
        {
            if (!gold.gameObject.transform.parent.gameObject.activeSelf)
            {
                gold.gameObject.transform.parent.gameObject.SetActive(true);
            }

            gold.text = Preferences.goldSlime.ToString() + '/' + PlayerPrefs.GetInt("Gold Slime Total");
        }
    }

    void OnApplicationQuit()
    {   
        // Total Slime Values
        PlayerPrefs.SetInt("Pink Slime", Preferences.pinkSlime);
        PlayerPrefs.SetInt("Blue Slime", Preferences.blueSlime);
        PlayerPrefs.SetInt("Gold Slime", Preferences.goldSlime);

        // Total Coin Values
        PlayerPrefs.SetInt("Pink Coin", Preferences.pinkCoin);
        PlayerPrefs.SetInt("Blue Coin", Preferences.blueCoin);
        PlayerPrefs.SetInt("Blue Coin", Preferences.goldCoin);

        // Coin Adder Values
        PlayerPrefs.SetInt("Pink Coin Add", Preferences.pinkCoinAdd);
        PlayerPrefs.SetInt("Blue Coin Add", Preferences.blueCoinAdd);
        PlayerPrefs.SetInt("Blue Coin Add", Preferences.goldCoinAdd);

        // Stored Slimes
        PlayerPrefs.SetInt("Stored Pink Slime", Preferences.storedPinkSlime);
        PlayerPrefs.SetInt("Stored Blue Slime", Preferences.storedBlueSlime);
        PlayerPrefs.SetInt("Stored Gold Slime", Preferences.storedGoldSlime);

        PlayerPrefs.SetString("Exit Time", DateTime.Now.ToBinary().ToString());
    }

    void LoadSaveData()
    {
        // Total Amount of Slimes Storage
        {
            if (!PlayerPrefs.HasKey("Pink Slime Total"))
            {
                PlayerPrefs.SetInt("Pink Slime Total", 10);
            }

            if (!PlayerPrefs.HasKey("Blue Slime Total"))
            {
                PlayerPrefs.SetInt("Blue Slime Total", 10);
            }

            if (!PlayerPrefs.HasKey("Gold Slime Total"))
            {
                PlayerPrefs.SetInt("Gold Slime Total", 10);
            }
        }

        // Number of Slime on Exit
        {
            if (!PlayerPrefs.HasKey("Pink Slime"))
            {
                PlayerPrefs.SetInt("Pink Slime", 0);
            }
            else
            {
                Preferences.pinkSlime = PlayerPrefs.GetInt("Pink Slime");
            }

            if (!PlayerPrefs.HasKey("Blue Slime"))
            {
                PlayerPrefs.SetInt("Blue Slime", 0);
            }
            else
            {
                Preferences.blueSlime = PlayerPrefs.GetInt("Blue Slime");
            }

            if (!PlayerPrefs.HasKey("Gold Slime"))
            {
                PlayerPrefs.SetInt("Gold Slime", 0);
            }
            else
            {
                Preferences.goldSlime = PlayerPrefs.GetInt("Gold Slime");
            }
        }

        //Blue or Gold Slime Availability
        {
            if (!PlayerPrefs.HasKey("Blue"))
            {
                PlayerPrefs.SetInt("Blue", 0);
                Preferences.blue = false;
            }
            else
            {
                Preferences.blue = Convert.ToBoolean(PlayerPrefs.GetInt("Blue"));
            }

            if (!PlayerPrefs.HasKey("Gold"))
            {
                PlayerPrefs.SetInt("Gold", 0);
                Preferences.gold = false;
            }
            else
            {
                Preferences.gold = Convert.ToBoolean(PlayerPrefs.GetInt("Gold"));
            }
        }

        // Coin Adder Values
        {
            if (!PlayerPrefs.HasKey("Pink Coin Add"))
            {
                PlayerPrefs.SetInt("Pink Coin Add", 1);
            }
            else
            {
                Preferences.pinkCoinAdd = PlayerPrefs.GetInt("Pink Coin Add");
            }

            if (!PlayerPrefs.HasKey("Blue Coin Add"))
            {
                PlayerPrefs.SetInt("Blue Coin Add", 1);
            }
            else
            {
                Preferences.blueCoinAdd = PlayerPrefs.GetInt("Blue Coin Add");
            }

            if (!PlayerPrefs.HasKey("Gold Coin Add"))
            {
                PlayerPrefs.SetInt("Gold Coin Add", 1);
            }
            else
            {
                Preferences.goldCoinAdd = PlayerPrefs.GetInt("Gold Coin Add");
            }

        }

        // Stored Slimes
        {
            if (!PlayerPrefs.HasKey("Stored Pink Slime"))
            {
                PlayerPrefs.SetInt("Stored Pink Slime", 0);
            }
            else
            {
                Preferences.storedPinkSlime = PlayerPrefs.GetInt("Stored Pink Slime");
            }

            if (!PlayerPrefs.HasKey("Stored Blue Slime"))
            {
                PlayerPrefs.SetInt("Stored Blue Slime", 0);
            }
            else
            {
                Preferences.storedBlueSlime = PlayerPrefs.GetInt("Stored Blue Slime");
            }

            if (!PlayerPrefs.HasKey("Stored Gold Slime"))
            {
                PlayerPrefs.SetInt("Stored Gold Slime", 0);
            }
            else
            {
                Preferences.storedGoldSlime = PlayerPrefs.GetInt("Stored Gold Slime");
            }
        }
    }
}
